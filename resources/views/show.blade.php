@extends('theme::layouts.main')

@section('content')

<x-profile::updateAvatar userId="{{ $user_id }}"
/>

<div class="container">
    <div class="row">
        <div class="col-12 col-md-3">
            <div class="d-flex justify-content-center">
                <div class="mt-4 m-2" style="width:150px;height:150px;">
                    <div style="
                        position: relative;
                        background-image: url({{url('/profile/show/avatar/md/').'/'.$avatar ?? '' }}); 
                        background-repeat:no-repeat; 
                        background-size: cover; 
                        width:100%;
                        padding-top:100%;
                        border-radius: 50%;";>
                        <div style="position:absolute; bottom:3px;right:3px;font-size:30px; 
                                    width:40px; height:40px; background-color:#ccc" 
                            class="rounded-circle">
                                <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#updateUserAvatar">
                                    <i style="font-size:25px; position: relative; left:8px; bottom:4px;" class="fas fa-camera-retro text-secondary"></i>
                                </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <div class="text-center m-2">
                    
                    {{$name ?? '' }}
                    {{$username ?? '' }}
                </div>
            </div>
            <div>
                <ul class="list-unstyled profile-menu text-center">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('profile')}}">
                            <i class="fas fa-fw fa-tachometer-alt"></i>
                            <span>Dashboard</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('logout')}}">
                            <i class="fas fa-sign-out-alt"></i>
                            <span>ออกจากระบบ</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-12 col-md-9 pt-4">
            <div class="row">
                <div class="col-lg-6 mb-4">
                    <div class="row mb-4">
                        <div class="col-12">
                            <div class="card shadow">
                                <div class="card-header py-3 d-flex justify-content-between">
                                    <x-system::cardHeaderForm 
                                        cardName="user"
                                        cardTitle="ข้อมูลบัญชี"
                                    />
                                </div>
                                <div class="card-body">
                                    <form id="user-edit-form" method="post" action="{{url('user/update')}}" class="needs-validation" novalidate>
                                        @csrf
                                        @if(session('user')['isAdmin'] ?? 0)
                                            <input type="hidden" name="user_id" value="{{$user_id}}">
                                        @endif
                                        <div class="d-flex mb-2">
                                            <div style="width: 130px;">ชื่อแสดง:</div> 
                                            <div class="user-edit-show">{{$name ?? '' }}</div>
                                            <div class="user-edit-form flex-fill" style="display: none;">
                                                <input class="w-100" value="{{$name ?? '' }}" name="name" />
                                            </div>
                                        </div>
                                        <div class="d-flex mb-2">
                                            <div style="width: 130px;">อีเมล:</div> 
                                            <div class="user-edit-show">{{$email ?? '' }}</div>
                                            <div class="user-edit-form flex-fill" style="display: none;">
                                                <input class="w-100" type="email" value="{{$email ?? '' }}" name="email" />
                                            </div>
                                        </div>
                                        <div class="d-flex mb-2">
                                            <div class="user-edit-form" style="width: 130px; display: none;">password:</div> 
                                            <div class="user-edit-form flex-fill" style="display: none;">
                                                <input class="w-100" type="password" value="" name="password" />
                                            </div>
                                        </div>
                                        <div class="d-flex mb-2">
                                            <div class="user-edit-form" style="width: 130px; display: none;">ยืนยัน password:</div> 
                                            <div class="user-edit-form flex-fill" style="display: none;">
                                                <input class="w-100" type="password" value="" name="password_confirmation" />
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="card shadow">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">เชื่อมต่อ Social</h6>
                                </div>
                                <div class="card-body">
                                    <x-theme::social own={{$own}}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card shadow">
                        <div class="card-header py-3 d-flex justify-content-between">
                            <x-system::cardHeaderForm 
                                cardName="profile"
                                cardTitle="ข้อมูลส่วนตัว"
                            />
                        </div>
                        <div class="card-body">
                            <form id="profile-edit-form" method="post" action="{{route('profile.update')}}" class="needs-validation" novalidate>
                                @csrf
                                @if(session('user')['isAdmin'] ?? 0)
                                    <input type="hidden" name="user_id" value="{{$user_id}}">
                                @endif
                                <div class="d-flex mb-2">
                                    <div style="width: 130px;">ชื่อ:</div> 
                                    <div class="profile-edit-show">{{$firstname ?? '' }} {{$lastname ?? '' }}</div>
                                    <div class="profile-edit-form flex-fill" style="display: none;">
                                        <input class="w-100" value="{{$firstname ?? '' }}" name="firstname" />
                                    </div>
                                </div>
                                <div class="d-flex mb-2">
                                    <div class="profile-edit-form" style="display: none; width: 130px;">นามสกุล:</div> 
                                    <div class="profile-edit-form flex-fill" style="display: none;">
                                        <input class="w-100" value="{{$lastname ?? '' }}" name="lastname" />
                                    </div>
                                </div>
                                <div class="d-flex mb-2">
                                    <div style="width: 130px;">โทรศัพท์:</div> 
                                    <div class="profile-edit-show">{{$phone ?? '' }}</div>
                                    <div class="profile-edit-form flex-fill" style="display: none;">
                                        <input class="w-100" value="{{$phone ?? '' }}" name="phone" />
                                    </div>
                                </div>
                                <div class="d-flex mb-2">
                                    <div style="width: 130px;">เพศ:</div> 
                                    <div class="profile-edit-show">{{$gender ?? '' }}</div>
                                    <div class="profile-edit-form" style="display: none;">
                                        <div class="form-check d-inline-block">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="gender" id="" value="no" {{$gender_no_check ?? ''}}>
                                            ไม่ระบุ
                                            </label>
                                        </div>
                                        <div class="form-check d-inline-block">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="gender" id="" value="male" {{$gender_male_check ?? ''}}>
                                            ชาย
                                            </label>
                                        </div>
                                        <div class="form-check d-inline-block">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="gender" id="" value="female" {{$gender_female_check ?? ''}}>
                                            หญิง
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex mb-2">
                                    <div class="profile-edit-form" style="{{$hide_aboutme}} width: 130px;">เกี่ยวกับฉัน:</div> 
                                    <div class="profile-edit-show">{{$aboutme ?? '' }}</div>
                                    <div class="profile-edit-form flex-fill" style="display: none;">
                                        <textarea name="aboutme" class="w-100" />{{$aboutme ?? '' }}</textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pt-3 pb-3">
        <div>{{$linenotify ?? '' }}</div>
        <div>{{$line_at ?? '' }}</div>
    </div>
</div>
@stop