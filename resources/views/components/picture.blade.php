@props(['user'=>$user])

<div style="
    position: relative;
    background-image: url(
       {{asset('/profile/show/avatar/'.$user->slug.'/md/').'/'.$user->avatar.'?v='.session()->get('imgVersion') ?? 1 }}
    ); 
    background-repeat:no-repeat; 
    background-size: cover; 
    width:100%;
    padding-top:100%;
    border-radius: 50%;";>
</div>