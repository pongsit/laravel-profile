{{-- <x-profile::aboutme :user=$user/> --}}
@props(['user'=>$user])
<div class="d-flex">
    <div class="@if(empty($user->profile->aboutme)) profile-edit-form @endif" 
         style="@if(empty($user->profile->aboutme)) display: none; @endif width: 130px;">เกี่ยวกับฉัน:</div> 
    <div class="profile-edit-show">{{$user->profile->aboutme ?? '' }}</div>
    <div class="profile-edit-form flex-fill" style="display: none;">
        <textarea name="aboutme" class="w-100 bg-dark text-white form-control mb-2" />{{$user->profile->aboutme ?? '' }}</textarea>
    </div>
</div>
  