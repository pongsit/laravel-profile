@props(['user'=>$user])
<link rel="stylesheet" href="{{url('/vendor/pongsit/system/croppie/croppie.min.css')}}" />
<script src="{{url('/vendor/pongsit/system/croppie/croppie.min.js')}}"></script>

<div class="modal authen_modal fade" id="updateUserAvatar" data-backdrop="static" tabindex="-1" aria-labelledby="loginLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header mb-2" style="border-bottom:none;padding:1rem 1rem 0 1rem;">
                <h4 class="text-center">รูปหลักของคุณ</h4>
                <a href="javascript:;" class="close text-secondary" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </a>
            </div>
            <div class="modal-body" style="padding:0rem 1rem 0rem 1rem;">
                <div class="row">
                    <div id="current_image" class="col-md-12 text-center mb-3">
                        <img class="rounded" src="{{asset('/profile/show/avatar/'.$user->slug.'/md/').'/'.$user->avatar.'?v='.session()->get('imgVersion') ?? 1 }}">
                    </div>
                    <div id="upload-preview" style="display:none;"></div>
                    <div class="col-md-12 text-center">
                        <label class="m-0 btn btn-outline-primary" style="display: inline-block;">
                            <span>เลือกรูป</span>
                            <input type="file" id="image_file" class="d-none">
                        </label>
                        <button class="btn btn-outline-primary upload-image" id="uploadButton">เรียบร้อย</button>
                    </div>
                </div>
                <form id="croppie" class="d-none" method="post" action="{{url('/profile/update/avatar')}}/{{$user->slug}}" enctype="multipart/form-data">
                  <input id="avatar" type="file" name="avatar">
                  @csrf
                </form>
                <style>
                    .croppie-container .cr-slider-wrap{
                        margin-bottom: 0;
                    }
                </style>
                <script>
                    $('#uploadButton').hide();

                    var resize = $('#upload-preview').croppie({
                        enableExif: true,
                        enableOrientation: true,    
                        viewport: {
                            width: 250,
                            height: 250,
                            type: 'circle',
                        },
                        boundary: {
                            width: 280,
                            height: 280
                        }
                    });
                    $('#image_file').on('click', function () { 
                        $('#upload-preview').show();
                        $('#current_image').hide();
                    });
                    $('#image_file').on('change', function () { 
                      var reader = new FileReader();
                      reader.onload = function (e) {
                        resize.croppie('bind',{
                          url: e.target.result
                        }).then(function(){
                          $('#uploadButton').show();
                        });
                      }
                      reader.readAsDataURL(this.files[0]);
                    });
                    $('.upload-image').on('click', function (ev) {
                        resize.croppie('result', {
                          type: 'blob',
                          size: "viewport",
                          quality : 1
                        }).then(function (img) {
                          let fileInputElement = document.getElementById('avatar');
                          let container = new DataTransfer();
                          let file = new File([img], "img.png",{type:"image/png", lastModified:new Date().getTime()});
                          container.items.add(file);
                          fileInputElement.files = container.files;
                          $('#croppie').submit();
                        });
                    });
                </script>
            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary d-none" data-bs-toggle="modal" data-bs-target="#updateUserAvatar">
    Update profile avatar
</button>
  