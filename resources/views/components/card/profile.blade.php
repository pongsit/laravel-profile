<div class="card shadow">
    <div class="card-header pt-3 pb-3 d-flex justify-content-between">
        <div><h6 class="m-0 font-weight-bold">Profile</h6></div>
    </div>
    <div class="card-body">
        <div class="d-flex justify-content-center">
            <div class="mt-4 m-2" style="width:150px;height:150px;">
                <a href="{{url('profile')}}@if(user()->can('manage_user'))/{{user()->slug}}@endif">
                    <div style="
                        position: relative;
                        background-image: url(
                           {{asset('/profile/show/avatar/'.user()->slug.'/md/').'/'.user()->avatar.'?v='.session()->get('imgVersion') ?? 1 }}
                        ); 
                        background-repeat:no-repeat; 
                        background-size: cover; 
                        width:100%;
                        padding-top:100%;
                        border-radius: 50%;";>
                    </div>
                </a>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <div class="text-center m-2">
                <div>{{user()->name ?? '' }}</div>
            </div>
        </div>
    </div>
</div>