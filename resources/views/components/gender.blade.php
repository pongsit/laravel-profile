{{-- <x-profile::gender :user=$user/> --}}
@props(['user'=>$user])
<div class="d-flex mb-2">
    <div class="@if(empty($user->profile->gender)) profile-edit-form @endif" 
         style="@if(empty($user->profile->gender)) display: none; @endif width: 100px;">เพศ:</div> 
    <div class="profile-edit-show">
        @if(!empty($user->profile->gender))
            @switch($user->profile->gender)
                @case('male')
                    ชาย
                    @break
                @case('female')
                    หญิง
                    @break
                @default
                    ไม่ระบุ
            @endswitch
        @endif
    </div>
    <div class="profile-edit-form text-start" style="display: none;">
        <div class="form-check d-inline-block">
            <label class="form-check-label">
            <input type="radio" class="form-check-input" name="gender" id="" value="no" 
                    @if(!empty($user->profile->gender)) @if(empty($user->profile->gender)) {{'checked'}} @endif @endif>
            ไม่ระบุ
            </label>
        </div>
        <div class="form-check d-inline-block">
            <label class="form-check-label">
            <input type="radio" class="form-check-input" name="gender" id="" value="male" 
                    @if(!empty($user->profile->gender))  @if($user->profile->gender == 'male') {{'checked'}} @endif @endif>
            ชาย
            </label>
        </div>
        <div class="form-check d-inline-block">
            <label class="form-check-label">
            <input type="radio" class="form-check-input" name="gender" id="" value="female" 
                    @if(!empty($user->profile->gender)) @if($user->profile->gender == 'female') {{'checked'}} @endif @endif>
            หญิง
            </label>
        </div>
    </div>
</div>