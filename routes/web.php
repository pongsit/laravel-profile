<?php

use Illuminate\Support\Facades\Route;
use Pongsit\Profile\Http\Controllers\ProfileController;

// profile
Route::get('/profile', [ProfileController::class, 'show'])->name('profile')->middleware('isLogin');
Route::get('/profile/show/avatar/{user:slug}/{size}/{name}', [ProfileController::class, 'showAvatar'])->name('profile.showAvatar');
Route::get('/profile/show/avatar/{user:slug}/{size}', [ProfileController::class, 'showDefaultAvatar'])->name('profile.showDefaultAvatar');
Route::get('/profile/show/avatar/{size}', [ProfileController::class, 'showDefaultAvatar'])->name('profile.showDefaultAvatar');
Route::post('/profile/update/avatar/{user:slug}', [ProfileController::class, 'updateAvatar'])->middleware('isLogin');
Route::post('/profile/update/{user:slug}', [ProfileController::class, 'update'])->name('profile.update')->middleware('isLogin');

// Route::middleware(['userCan:manage_user'])->group(function () {
    Route::get('/profile/{user:slug}', [ProfileController::class, 'show'])->name('profile.user');
// });

// Route::middleware(['userCan:manage_profile'])->group(function () {
//     Route::get('/profile/setting', [ProfileController::class, 'setting'])->name('profile.setting');
// });