<?php

namespace Pongsit\Profile;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Pongsit\Profile\Providers\EventServiceProvider;

class ProfileServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/profile.php', 'services'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['router']->namespace('Pongsit\\Profile\\App\\Controllers')
                ->middleware(['web'])
                ->group(function () {
                    $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
                });
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'profile');
        // $this->publishes([
        //     __DIR__.'/../public' => public_path('vendor/profile'),
        // ], 'public');
    }
}