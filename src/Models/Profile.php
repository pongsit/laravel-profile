<?php

namespace Pongsit\Profile\Models;

use Pongsit\Profile\Database\Factories\ProfileFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
  use HasFactory;

  // Disable Laravel's mass assignment protection
  protected $guarded = [];

  protected static function newFactory()
  {
      return ProfileFactory::new();
  }

  public function user(){
    return $this->belongsTo('Pongsit\User\Models\User');
  }

 //  public function user($user_id=0){
	// 	return Profile::where('user_id', $user_id)->first();
	// }
}
