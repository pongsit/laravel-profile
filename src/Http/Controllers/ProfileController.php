<?php

namespace Pongsit\Profile\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use Response;
use Throwable;
use Image;
use Pongsit\Profile\Models\Profile;
use Pongsit\Role\Models\Role;
use Pongsit\Role\Http\Controllers\RoleController;
use Pongsit\System\Models\System;
use Pongsit\User\Models\User;

class ProfileController extends Controller
{	
    public function show(User $user){

    	if(!user()->isLogin()){
			return redirect()->route('login');
		}
        $variables = [];
		if(user()->can('manage_user') && !empty($user->id)){
    		$variables['user'] = $user;
    	}else{
    		$variables['user'] = user();	
    	}

    	// dd($variables['user']->getUserRole()->sortByDesc('power')->first());

    	$variables['own'] = 0;
    	if(user()->id == $variables['user']->id){
    		$variables['own'] = 1;
    	}
    	// dd($variables['user']->genderIsSecret);

    	// dd($variables);

		if(view()->exists('profile.show')){
			return view('profile.show',$variables);
		}else{
			return view('profile::show',$variables);
		}
	}

	public function showAvatar(User $user,$size,$file_name){
		$path = storage_path('app/public/user/'.$user->id.'/profile/avatar/'.$size.'/'.$file_name);

        if (!File::exists($path)) {
            return $this->showDefaultAvatar();
        }

        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
	}

	public function showDefaultAvatar(){
		$path = public_path('vendor/pongsit/system/img/avatar.png');

		if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
	}

	public function updateAvatar(Request $request, User $user){

		$request->validate([
			'avatar'=> 'required|image|mimes:jpeg,png,jpg',
		],[
			'avatar.required'=>'กรุณาเลือกภาพที่ต้องการอัพโหลด',
			'avatar.image'=>'กรุณาเลือกภาพ',
			'avatar.mimes'=>'โดยใช้นามสกุล .jpg, .jpeg หรือ .png เท่านั้น',
		]);

		$inputs = $request->all();
		unset($inputs['_token']);

		$file_name = $inputs['avatar']->hashName();	
		// $file_name = user()->id.'_'.time().'_'.rand(100,999).'.png';

		if(!user()->can('manage_user')){
			$user = user();
		}

		if(Storage::exists('public/user/'.$user->id.'/profile/avatar')){
            Storage::deleteDirectory('public/user/'.$user->id.'/profile/avatar');
		}

       	try{
			Storage::put('public/user/'.$user->id.'/profile/avatar/original/',$request->file('avatar'));
			Storage::put('public/user/'.$user->id.'/profile/avatar/sm/',$request->file('avatar'));
			Storage::put('public/user/'.$user->id.'/profile/avatar/md/',$request->file('avatar'));

			$sm_path = storage_path('app/public/user/'.$user->id.'/profile/avatar/sm/'.$file_name);
			$md_path = storage_path('app/public/user/'.$user->id.'/profile/avatar/md/'.$file_name);

			$slider = Image::make($sm_path);
			$slider->fit(150);
			$slider->save();

			$slider = Image::make($md_path);
			$slider->fit(300);
			$slider->save();

		}catch(Throwable $e){
			dd($e);
			return back()->with(['error'=>'กรุณาลองใหม่อีกครั้ง']);
		}

		$user->avatar = $file_name;
		$user->save();

		// try{
		// 	User::where('id', $user->id)->update([
		// 		'avatar' => $file_name
		// 	]);
		// }catch(Throwable $e){
		// 	return back()->with(['error'=>'กรุณาลองใหม่อีกครั้ง']);
		// }

		if(user()->id == $user->id){
			session(['user'=>user($user->id)]);
		}
		
        return back()->with(['success'=>'เปลี่ยนภาพใหม่เรียบร้อย','imgVersion'=>time()]);
    }

	public function update(Request $request, User $user){

		$inputs = $request->all();
		unset($inputs['_token']);

		if(!user()->can('manage_user')){
			$user = user();	
		}

		$inputs['user_id'] = $user->id;

		$profile = Profile::updateOrCreate(['user_id'=>$user->id],$inputs);

		if(user()->id == $user->id){
			session(['user'=>user($user->id)]);
		}

		return back()->with(['success'=>'ปรับปรุงข้อมูลเรียบร้อย']);

	}
}
